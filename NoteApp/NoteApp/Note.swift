//
//  Note.swift
//  NoteApp
//
//  Created by 劉家維 on 2018/7/11.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import Foundation
import UIKit


class RedNote {
    var text : String?
    var image : UIImage?
    var redNoteID : String
    init() {
        redNoteID = UUID().uuidString
    }
}
