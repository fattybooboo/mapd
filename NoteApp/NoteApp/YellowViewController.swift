//
//  YellowViewController.swift
//  NoteApp
//
//  Created by 劉家維 on 2018/7/11.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit
//2018.07.20
//插頁廣告
import GoogleMobileAds


class YellowViewController: UIViewController, GADInterstitialDelegate {
    //2018.07.20
    //插頁廣告
    var interstitisl : GADInterstitial?

    @IBOutlet weak var yellowTextView: UITextView!
    @IBOutlet weak var yellowImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        //2018.07.20
        //插頁廣告
        self.interstitisl = GADInterstitial(adUnitID: "ca-app-pub-1521801495507235/2542793906")
        self.interstitisl?.delegate = self
        self.interstitisl?.load(GADRequest())
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    //MARK: done Methods.
    @IBAction func doneBtw(_ sender: UIBarButtonItem) {
        //2018.07.20
        //插頁廣告
        
        if let adview  = self.interstitisl, adview.isReady {//如果廣告存在,而且是ready狀態,顯示廣告
            adview.present(fromRootViewController: self)
        }else {
            
            self.navigationController?.popViewController(animated: true)
        }
       
    }
    //MARK: camera Methods.
    @IBAction func camera(_ sender: UIBarButtonItem) {
        
    }
    
    //2018.07.20
    //插頁廣告
    //MARK: - GADInterstitialDelegate.
    
    //使用者按下x關閉廣告
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //使用者點擊廣告,可能會導向app stone 進行下載
    func interstitialWillLeaveApplication(_ ad: GADInterstitial) {
        self.dismiss(animated: true) {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    //////////////////////////////////////////
}


extension YellowViewController {
    
}
