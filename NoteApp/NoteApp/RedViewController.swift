//
//  ViewController.swift
//  NoteApp
//
//  Created by 劉家維 on 2018/7/11.
//  Copyright © 2018年 劉家維. All rights reserved.
//

import UIKit

import StoreKit
import MessageUI

import Firebase

//2018.07.20
//廣告
import GoogleMobileAds

class RedViewController: UIViewController, MFMailComposeViewControllerDelegate,GADBannerViewDelegate {
    
    
    //2018.07.20
    //廣告
    var bannerView: GADBannerView?
    
    var redData : [RedNote] = []
    
    @IBOutlet weak var tableTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var redTableView: UITableView!
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        for index in 1...10 {
            let fakeNote = RedNote()
            fakeNote.text = "Note \(index)"
            redData.append(fakeNote)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //使用stotyboard 設定 sourceData,Delegate.
        //設定 navigationItem.leftBarButtonItem.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        
        //2018.07.20
        //廣告
        self.bannerView = GADBannerView(adSize: kGADAdSizeBanner)
//        self.bannerView?.translatesAutoresizingMaskIntoConstraints = false
        self.bannerView?.adUnitID = "ca-app-pub-1521801495507235/5775461901" //廣告單元編號
        self.bannerView?.delegate = self
        self.bannerView?.rootViewController = self
        self.bannerView?.load(GADRequest())
        
        //2018.07.20
        //廣告
//        self.redTableView.tableHeaderView = self.bannerView
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - add Data.
    @IBAction func redAddNote(_ sender: UIBarButtonItem) {
        
        //2018.07.20
        //測試Debug.
//        let array : [String] = []
//        array[0]

        //2018.07.20
        //統計按鈕被按了幾次.
        Analytics.logEvent("addNote", parameters: nil)
        
        //統計買了什麼.
        Analytics.logEvent(AnalyticsEventEcommercePurchase, parameters: [AnalyticsParameterQuantity:1, AnalyticsParameterPrice:2])
        
        let newNote : RedNote = RedNote()
        newNote.text = NSLocalizedString("new.note", comment: "New Note")
        self.redData.insert(newNote, at:0 )
        let indexPaths = IndexPath(row: 0, section: 0)
        self.redTableView.insertRows(at: [indexPaths], with: .automatic)
    }
    
    //MARK: - navigationItem Methods.
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        self.redTableView.setEditing(editing, animated: true)
    }
    
    //MARK: - delete Data.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            self.redData.remove(at: indexPath.row )
            self.redTableView.deleteRows(at: [indexPath], with: .automatic)
        }
    }
    
    
    //MARK: - Segue.
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //....
    }
    
    
    //2018.07.20
    //評分系統
    @IBAction func askForRating(){
        
        let askController = UIAlertController(title: "Hello App User", message: "If you like this app,please rate in App Store. Thanks.", preferredStyle: .alert)
        let laterAction = UIAlertAction(title: "稍後再評", style: .default, handler: nil)
        askController.addAction(laterAction)
        let okAction = UIAlertAction(title: "我要評分", style: .default) { (action) -> Void in
            let appID = "436107488"
            let appURL = URL(string: "https://itunes.apple.com/us/app/itunes-u/id\(appID)?action=write-review")!
            UIApplication.shared.open(appURL, options: [:], completionHandler: { (success) in
                
            })
        }
        askController.addAction(okAction)
        self.present(askController, animated: true, completion: nil)
        
        
    }
    
    @IBAction func stoneBtn(_ sender: Any) {
        SKStoreReviewController.requestReview()
    }
    
    
//2018.07.20
//EmailSupport
    @IBAction func support(){
        //手機是否有設定email帳號
        if ( MFMailComposeViewController.canSendMail()){
            let alert = UIAlertController(title: "", message: "We want to hear from you, Please send us your feedback by email in English", preferredStyle: .alert)
            let email = UIAlertAction(title: "email", style: .default, handler: { (action) -> Void in
                let mailController =  MFMailComposeViewController()
                mailController.mailComposeDelegate = self
                mailController.title = "I have question" //email title, controller's title
                mailController.setSubject("I have question")//email 標指
                let version = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")//產品的版本
                let product = Bundle.main.object(forInfoDictionaryKey: "CFBundleName")//取得產品名稱
                let messageBody = "<br/><br/><br/>Product:\(product!)(\(version!))"//支援html, 所以<br>作為換行
                mailController.setMessageBody(messageBody, isHTML: true)//email內文,html設成true
                mailController.setToRecipients(["support@yoursupportemail.com"])//設定收件人,自己的email.
                self.present(mailController, animated: true, completion: nil)
            })
            alert.addAction(email)
            self.present(alert, animated: true, completion: nil)
        }else{
            //alert user can't send email
        }
    }
    
    //MARK: MFMailComposeViewControllerDelegate
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        switch (result){
        case MFMailComposeResult.cancelled:
            print("user cancelled")
        case MFMailComposeResult.failed:
            print("user failed")
        case MFMailComposeResult.saved:
            print("user saved email")
        case MFMailComposeResult.sent:
            print("email sent")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    //2018.07.20
    //廣告
    //MARK: - GADBannerViewDelegate.
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        //第一次收到廣告,還沒貼到controller's view上,所以superview是空的
        if bannerView.superview == nil {
            
//            self.redTableView.tableHeaderView =  bannerView
    self.bannerView?.translatesAutoresizingMaskIntoConstraints = false
            self.tableTopConstraint.isActive = false //關閉table上緣的條件,插入banner在table上方
            self.view.addSubview(bannerView)
            
            self.bannerView?.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
            self.bannerView?.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
            self.bannerView?.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true//廣告上緣 = safearea的上緣
            self.bannerView?.bottomAnchor.constraint(equalTo: self.redTableView.topAnchor).isActive = true//廣告下緣 = tableview上緣
            
            
        }
    }

    
    
    
/////////////////////////////////////////////
}


extension RedViewController : UITableViewDataSource, UITableViewDelegate {
    
    //MARK: - UITableViewDataSource Methods.
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return redData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = redTableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let indexPaths = redData[indexPath.row]
        cell.textLabel?.text = indexPaths.text
        //2018.07.16
//        let now = Date()
        
        //民國歷
//        let formatter = DateFormatter()
//        formatter.dateStyle = .short
//        formatter.timeStyle = .short
//        formatter.calendar = Calendar(identifier: .republicOfChina)
//        cell.detailTextLabel?.text = formatter.string(from: now)

        
        //預設西歷
//        cell.detailTextLabel?.text = DateFormatter.localizedString(from: now, dateStyle: .short, timeStyle: .short)
        
        //2018/07/16
        //數字轉換
        cell.detailTextLabel?.text = NumberFormatter.localizedString(from: 1234.56, number: .currency)
        
        
        
        return cell
    }
    
    //MARK: - UITableViewDelegate Methods.
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.redTableView.deselectRow(at: indexPath, animated: true)
    }
    
}
